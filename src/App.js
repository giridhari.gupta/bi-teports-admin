//router
// import IndexRouters from "./router/index"

//scss
import "./assets/scss/hope-ui.scss";
import "./assets/scss/custom.scss";
import "./assets/scss/dark.scss";
import "./assets/scss/rtl.scss";
import "./assets/scss/customizer.scss";

// Redux Selector / Action
import { useDispatch } from "react-redux";

// import state selectors
import { setSetting } from "./store/setting/actions";
import RouteIdentifier from "./router/RouteIdentifier";
import routes from "./router/router";

function App({ children }) {
  // const dispatch = useDispatch()
  // dispatch(setSetting())
  return (
    <div className="App">
      {/* <IndexRouters /> */}
      {children}
      <RouteIdentifier routes={routes} />
    </div>
  );
}

export default App;
