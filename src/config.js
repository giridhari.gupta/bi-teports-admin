export const REDUX_PERSIST_KEY = "bi-reports-admin"

export const NODE_ENV = "development";

export const API_URL =
    NODE_ENV === "production"
        ? "https://api-securetext-staging.weavers-web.com/v1"
        : NODE_ENV === "development"
            ? "https://api-securetext.weavers-web.com/v1"
            : "http://localhost:4000/v1";