import Index from "../views/dashboard/index";
import SignIn from "../views/dashboard/auth/sign-in";
import Default from "../layouts/dashboard/default";
import SignUp from "../views/dashboard/auth/sign-up";
import ConfirmMail from "../views/dashboard/auth/confirm-mail";
import LockScreen from "../views/dashboard/auth/lock-screen";
import Recoverpw from "../views/dashboard/auth/recoverpw";
import UserProfile from "../views/dashboard/app/user-profile";
import UserAdd from "../views/dashboard/app/user-add";
import UserList from "../views/dashboard/app/user-list";
import UserLayout from "../components/common/layouts/UserLayout";
const VisitorLayoutFunc = (comp) => {
  return <UserLayout>{comp}</UserLayout>;
};
const routes = [
  // Auth routes
  {
    path: "/",
    element: <SignIn />,
    protected: false,
    role: [],
  },
  {
    path: "/auth/sign-in",
    element: <SignIn />,
    protected: false,
    role: [],
  },
  {
    path: "/dashboard",
    element: <Default />,
    protected: false,
    role: [],
  },
  {
    path: "/auth/sign-up",
    element: <SignUp />,
    protected: false,
    role: [],
  },
  {
    path: "/auth/confirm-mail",
    element: <ConfirmMail />,
    protected: false,
    role: [],
  },
  {
    path: "/auth/lock-screen",
    element: <LockScreen />,
    protected: false,
    role: [],
  },
  {
    path: "/auth/recoverpw",
    element: <Recoverpw />,
    protected: false,
    role: [],
  },
  {
    path: "dashboard/app/user-profile",
    element: VisitorLayoutFunc(<UserProfile />),

    protected: false,
    role: [],
  },
  {
    path: "dashboard/app/user-add",
    element: VisitorLayoutFunc(<UserAdd />),
    protected: false,
    role: [],
  },
  {
    path: "dashboard/app/user-list",
    element: VisitorLayoutFunc(<UserList />),
    protected: false,
    role: [],
  },
];
export default routes;
